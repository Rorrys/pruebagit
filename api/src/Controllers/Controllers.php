<?php

namespace Controllers;

require_once('phpmailer/phpmailer/PHPMailerAutoload.php');

require_once('Login/loginController.php');
require_once('Login/usersController.php');
require_once('Login/registerController.php');
require_once('Login/forgotController.php');

require_once('Sait/ciaController.php');
require_once('Sait/artsController.php');
require_once('Sait/lineasController.php');

use PHPMailer;

class Controllers
{

	public function Consultar($that,$query){
		$sth = $that->db->prepare($query);
	    $sth->execute();
        $result = $sth->fetchAll();
        return $result;
	}

	public function validarCorreo($that,$correo){

		$query = "Select * from usuarios where email='".$correo."' and estatus<>3";
		
        $sth = $that->db->prepare($query);
	    $sth->execute();
	    $result = $sth->fetchAll();

        if($result){
            $dataCorreo= array(
                'rfc'     	=>$result[0]['rfc'], 
                'nomuser' 	=>$result[0]['nomuser'],
                'password' 	=>$result[0]['password'],
                'apellido' 	=>$result[0]['apellido'],
                'email' 	=>$result[0]['email'],
                'numuser' 	=>$result[0]['id'],
                'estatus' 	=>$result[0]['estatus'],
                'numsuc' 	=>$result[0]['numsuc'],
                'md3admin'    =>$result[0]['md3admin'],
                'pasar' 	=> 'Si',
                'existe'	=> 'Si'
            );
        }

        if(!isset($dataCorreo)){
            $dataCorreo = array('existe' => 'No' );
        }
        return $dataCorreo;
	}



    public function validarRfc($that,$rfc){

        $query = "Select * from conector where rfc='".$rfc."'";
        $sth = $that->db->prepare($query);
        $sth->execute();
        $result = $sth->fetchAll();

        if($result){
           $dataRFC= array(
                'rfc'     =>$result[0]['rfc'], 
                'nomcia'  =>$result[0]['nomcia'], 
                'pasar' => 'Si',
                'existe' => 'Si'
            );
        }
    
        if(!isset($dataRFC)){
            $dataRFC = array('existe' => 'No' );
        }

        return $dataRFC;

    }

    
    public function validarUser($that,$rfc,$email){

        $existeRFC   = Controllers::validarRFC($rfc);
        $existeEmail = Controllers::validarCorreo($email);

        $query = "Select * from usuarios where rfc='".$rfc."' and email='".$email."' and estatus<>3";
        $sth = $that->db->prepare($query);
        $sth->execute();
        $result = $sth->fetchAll();

        if($result){
            $data= array(
                'rfc'       =>$result[0]['rfc'], 
                'nomuser'   =>$result[0]['nomuser'],
                'apellido'  =>$result[0]['apellido'],
                'email'     =>$result[0]['email'],
                'numuser'   =>$result[0]['id'],
                'estatus'   =>$result[0]['estatus'],
                'pasar'     => 'Si'
            );
        }

        if(isset($data)){
            return $data;
        }

    }

    
    public function enviarCorreo($that,$FromName,$title,$subject,$body,$data,$email,$nomuser){
       
        $mail = new PHPMailer; 
        $mail->IsSMTP();
        $mail->Host = "mail.omnis.com";
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'admin@md3.mx';                 // SMTP username
        $mail->Password = 'Indicadores.2017';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587; 

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );       

        $mail->From = "admin@md3.mx";
        $mail->FromName = $FromName;
        $mail->addAddress($email, $nomuser);
        $mail->addAddress($email); 
        $mail->addReplyTo($email, "No Responder");
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AltBody = $title;
        $mail->CharSet = 'UTF-8';

        if(!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } 
        else {
            echo json_encode($data);
        }
    }

    

};


?>