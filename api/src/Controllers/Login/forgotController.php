<?php

namespace forgotController;

use Interop\Container\ContainerInterface;
use Controllers\Controllers;

class forgotController
{

	public $db;
   
   	public function __construct(ContainerInterface $data) {
       $this->db = $data->get('db');
       $this->apiWeb = $data->get('apiWeb');
   	}
   	
	public function ForgotPass ($request,$response,$args) {
		$that = $this;
   		$email  = $request->getAttribute('email');

        //validamos que el usuario exista
        $cUsuario = Controllers::validarCorreo($that,$email);
        $existeCorreo = $cUsuario['existe'];

        if($existeCorreo=='Si'){ //si el usuario que esta pidiendo la solictud existe

        	//creamos el codigo de seguridad a enviar por correo
       		$codigo = $this->crearCodigoSeguridad($cUsuario);
            $data = $cUsuario;

            //Si nisiquiera esta activado su correo que lo active
            if($data['estatus']=='1'){
                echo ('Oye ni siquiera has activado tu cuenta!');  
            } else if($codigo!='No'){ //Si se genero correctamente su codigo de seguridad
                $this->crearCorreoPassword($data,$codigo);
            } 
            
        } else { //Si el usuario que esta pidiendo la solicitud ni siquiera existe pos le decimos que se de de alta
            $data = array('email' => 'No');
            echo json_encode($data);
        }
	}



	public function NewCodigo ($request,$response,$args) {

		//defino variables
		$that = $this;
	    $numuser  = $request->getAttribute('numuser');
	    $codigo   = $request->getAttribute('codigo');
	    $rfc2   = $request->getAttribute('rfc2');
	    $email    = $request->getAttribute('email');

	    //valido si se encuentra su usuario
	    $pasar = Controllers::validarUser($that,$rfc2,$email);

	    //Si existe su usuario
	    if(isset($pasar['pasar'])){

	        //si el numero de usuario que me esta mandando concuerda con el de la base de datos
	        if($pasar['numuser']==$numuser) {

	            $query = "Select * from usuarios where codigo='".$codigo."'";
	            $sth = $this->db->prepare($query);
		        $sth->execute();
		        $result = $sth->fetchAll();

	            if($result){
                    $data= array(
                        'rfc'     =>$result[0]['rfc'], 
                        'nomuser' =>$result[0]['nomuser'],
                        'apellido' =>$result[0]['apellido'],
                        'email' =>$result[0]['email'],
                        'numuser' =>$result[0]['id'],
                        'estatus' =>$result[0]['estatus'],
                        'pasar' => 'Si'
                    );
	            }
	        }   
	    } 

	    if(isset($data)){
	        echo json_encode($data);
	    } else{
	        $data = array('codigo' => 'No' , );
	        echo json_encode($data);
	    }

	}

	public function NewPass ($request,$response,$args) {
   		//$data = $request->getQueryParams();
	    $data = $request->getParsedBody();
	    //$id  = $request->getAttribute('id');
	    
	    $id       = $data['id'];
	    $password = $data['password'];

	    $query = "UPDATE usuarios SET password='".$password."' WHERE id=".$id;
	    $sth = $this->db->prepare($query);
        $sth->execute();
	    echo json_encode($data);  

	}

	public function crearCodigoSeguridad($data){

        $codigo  = 'No';
        $fecha   = date("Ymd");
        $hora    = date("His");
        $numuser = $data['numuser'];
        $codigo  = $numuser.$fecha;
        $query = "UPDATE usuarios SET codigo='".$codigo."' where id=".$numuser;
        $sth = $this->db->prepare($query);
        $sth->execute();
        return $codigo;
       
    }

    public function crearCorreoPassword($data,$codigo){

    	$apiWeb = $this->apiWeb;
	    //El nombre de quien lo envia
	    $FromName = "Md3Admin";

	    //Asunto
	    $subject = "Cambio Contraseña";

	    //Encabezado del correo
	    $title   = "Hola ".$data['nomuser']." ".$data['apellido'];

	    //cuerpo del correo
	     $body     ="
	                <a>
	                    <img src='".$apiWeb."img/contrasena.png' style='width:800px'/>
	                </a>
	                <br>
	                <h3>Tu Codigo de Seguridad es: <strong> ".$codigo."</strong></h3>
	                ";

	    $email = $data['email'];
	    $nomuser = $data['nomuser'];
	    
	    //Le enviamos el correo
	    Controllers::enviarCorreo($that,$FromName,$title,$subject,$body,$data,$email,$nomuser);

	}
	

};


?>