<?php

namespace loginController;

use Interop\Container\ContainerInterface;
use Controllers\Controllers;

class loginController
{
   
	public $db;
   
   	public function __construct(ContainerInterface $data) {
       $this->db = $data->get('db');
       $this->dataCia = $data->get('data');
   	}

	public function Login ($request,$response,$args) {
		$that = $this;
	   $data = $request->getParsedBody();
	   //$data = $request->getQueryParams();

	   //Si esta entrando desde el Login de Indicadores comiendo hacer validaciones
	   if(!empty($data['email']) && !empty($data['password'])) {

	        $correo     = $data['email'];
	        $contrasena = $data['password'];
	        
	        //Primero valido correo si esta registrado
	        $existeCorreo = Controllers::validarCorreo($that,$correo);

	        //si esta registrado entonces busco los datos del usuario
	        if(isset($existeCorreo['pasar'])){

	        	$data = $existeCorreo;
	        	$dataCia = $this->dataCia;
	        	$nomcia = $dataCia['nomcia'];

	        	//Si la contraseña que puso es correcta lo dejamos pasar
	            if($contrasena==$data['password']){

	            	$empresa = Controllers::validarRfc($that,$data['rfc']);

	            	if($data['md3admin']==0){

	            		$respuesta = array(
		                    'jwt'      => '29bf081nfg79nf1f397fes731ifn', 
		                    'rfc'      => $data['rfc'], 
		                    'nomuser'  => $data['nomuser'],
		                    'apellido' => $data['apellido'],
		                    'numuser'  => $data['numuser'],
		                    'email'    => 'Si',
		                    'estatus'  => $data['estatus'],
		                    'numsuc'   => $data['numsuc'],
		                    'nomcia'   => $empresa['nomcia'],
		                    'md3admin' => 'No',
		                    'password' => 'Si'
		                );

		                echo json_encode($respuesta);


	            	}else{

		                $respuesta = array(
		                    'jwt'      => '29bf081nfg79nf1f397fes731ifn', 
		                    'rfc'      => $data['rfc'], 
		                    'nomuser'  => $data['nomuser'],
		                    'apellido' => $data['apellido'],
		                    'numuser'  => $data['numuser'],
		                    'email'    => 'Si',
		                    'estatus'  => $data['estatus'],
		                    'numsuc'   => $data['numsuc'],
		                    'nomcia'   => $empresa['nomcia'],
		                    'md3admin' => 'Si',
		                    'password' => 'Si'
		                );

		                echo json_encode($respuesta);
	            	}

	            }else{ //Si su contraseña no fue correcta entonces le digo que esta incorrecta

	                $respuesta = array(
	                    'email'   => 'Si',
	                    'estatus' => $data['estatus'],
	                    'password' => 'No'
	                );
	                echo json_encode($respuesta);
	            }

	        } else { //Si no encontro ningun usuario con ese correo ledigoque tiene que registrarse primero

	            $respuesta = array('email'   => 'No');
	            echo json_encode($respuesta);            
	        }
	     
	   }
	   else { //Si intento entrar hackeando no lo dejo hacer nada
	       echo '{"error":{"text":"Username and Password are required."}}';
	   }

	}

};


?>