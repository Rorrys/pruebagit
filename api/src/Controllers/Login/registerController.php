<?php

namespace registerController;

use Interop\Container\ContainerInterface;
use Controllers\Controllers;

class registerController
{
   	public $db;
	public $db2;
	public $apiWeb;
	
   	public function __construct(ContainerInterface $data) {
       $this->db = $data->get('db');
       $this->db2 = $data->get('db2');
       $this->apiWeb = $data->get('apiWeb');
   	}

	public function Register ($request,$response,$args) {
		$that = $this;
	    $data = $request->getParsedBody();
	    $result = 'No';
	    $correo     = $data['email'];
	    $contrasena = md5($data['passWord']);
	    $nomuser    = $data['nomuser'];
	    $rfc        = $data['rfc'];
	    $telefono   = $data['telefono'];
	    $apellido   = $data['apellido'];
	    $fecha      = date("y.m.d");

	    $arrayName = array('correo' => $correo,
	      'nomuser' => $nomuser,
	      'apellido' => $apellido 
	    );

	    $siExiste2 = Controllers::validarCorreo($that,$correo);
	    $siExiste3 = Controllers::validarRFC($that,$rfc);
	    $existeCorreo = $siExiste2['existe'];
	    $existeRFC    = $siExiste3['existe'];

	    if($existeCorreo=='No' and $existeRFC=='Si'){ 

	    	$query = "INSERT INTO usuarios(email,nomuser,password,rfc,apellido,telefono,fecha) VALUES('".$correo."','".$nomuser."','".$contrasena."','".$rfc."','".$apellido."','".$telefono."','".$fecha."')";
	    	$sth = $this->db->prepare($query);
	        $sth->execute();
	        if($sth){ $result = 'Si'; }
		}

		if($result=='Si'){

			$apiWeb = $this->apiWeb;
	        $data = array('rfc' => 'Si' ,'email' => 'No');
	        $FromName = "Sistema de Indicadores";
	        $title    = "Bienvenido ".$nomuser." ".$apellido;
	        $subject  = "Confirmacion de Correo";
	        $body     ="
	                    <a href='".$apiWeb."apiSait/public/".$rfc."/login/confirm/user/".$correo."'>
	                        <img src='".$apiWeb."img/confirmar.png' style='width:800px'/>
	                    </a>
	                    ";
	        Controllers::enviarCorreo($that,$FromName,$title,$subject,$body,$data,$correo,$nomuser);
	    } else {
	   
	        $datos = array(
	            'rfc' => $siExiste3 ,
	            'email' => $siExiste2 
	        );     
	        echo json_encode($datos);
	    }
	
	}

	public function ConfirmEmail($request,$response,$args){

		$that    = $this;
		$rfc     = $request->getAttribute('rfc');
	    $email   = $request->getAttribute('email');
	    $fechaHoy = date("y.m.d");

	    $sth = $that->db->prepare("Select * from usuarios where rfc=:rfc and email=:email");
	    $sth->bindParam("rfc",$rfc);
	    $sth->bindParam("email",$email);	    
	    $sth->execute();
        $result = $sth->fetchAll();
        if($result){
        	$data = array(
	            'numuser'=>$result[0]['id'],
	            'email'=>'Si',
	            'rfc'=>'Si', 
	            'estatus'=>$result[0]['estatus'],
	            'fecha'=>$result[0]['fecha']
	        );
        }
        
	   
	    $siExiste2 = Controllers::validarCorreo($that,$email);
	    $siExiste3 = Controllers::validarRFC($that,$rfc);
	    $existeCorreo = $siExiste2['existe'];
	    $existeRFC    = $siExiste3['existe'];

	    $apiWeb = $this->apiWeb;

	    if($existeRFC=='No'){
	         header('Location: '.$apiWeb.'gracias/rfc.html');
	    }

	    if($existeCorreo=='No'){
	         header('Location: '.$apiWeb.'gracias/email.html'); 
	    }

	    if(isset($data)){

	        if($data['estatus']=='2'){
	             header('Location: '.$apiWeb.'gracias/estatus.html'); 
	        } else {
			    $sth = $that->db->prepare("UPDATE usuarios SET estatus=:estatus WHERE id=:id");
			    $sth->bindParam("estatus",$estatus);
			    $sth->bindParam("id",$data['numuser']);	    
			    if($sth->execute()){
			    	$actualizado = 'Si';
			    }
	        }
	    } else {
	         header('Location: '.$apiWeb.'gracias/usuario.html');
	    }

	    if(isset($actualizado)){
	        header('Location: '.$apiWeb.'gracias');
	    } 
	}

};

?>