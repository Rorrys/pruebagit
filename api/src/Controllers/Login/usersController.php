<?php

namespace usersController;

use Interop\Container\ContainerInterface;
use Controllers\Controllers;

class usersController
{
   
	public $db;
   
   	public function __construct(ContainerInterface $data) {
       $this->db = $data->get('db');
   	}

	public function Users ($request,$response,$args) {
		$that = $this;
		$result = Controllers::Consultar($that,'SELECT * FROM usuarios');
	    echo json_encode($result);
	}

	public function UsersById($request,$response,$args){
		$that = $this;
		$id = $request->getAttribute('id');
		$result = Controllers::Consultar($that,'SELECT * FROM usuarios WHERE id='.$id);
	
		if(!$result){
	        $result[] = [
	            "nomuser"   => "",
	            "rfc"       => "",
	            "email"  => ""
	        ];
	    }
	    
	    echo json_encode($result);
	}

};


?>