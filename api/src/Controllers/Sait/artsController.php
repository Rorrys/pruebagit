<?php

namespace artsController;

use Interop\Container\ContainerInterface;
use Controllers\Controllers;

class artsController
{
   
	public $db;

   	public function __construct(ContainerInterface $data) {
       $this->db = $data->get('db');
   	}

	public function arts ($request,$response,$args) {
		$that = $this;
		$result = Controllers::Consultar($that,'SELECT * FROM arts');
	    echo json_encode($result);
	}

	public function artById ($request,$response,$args) {
		$that = $this;
		$numart = $request->getAttribute('id');
		$result = Controllers::Consultar($that,'SELECT * FROM arts WHERE numart='.$numart);

		$foto = $result[0]['foto'];
		if($foto!=''){
			//Quitar decimales
			$result[0]['precio1'] = number_format($result[0]['precio1'], 2, '.', ',');
			$result[0]['foto'] = 'img/arts/' . $foto;
		}

	    echo json_encode($result);
	}

	public function artByFotos ($request,$response,$args) {

		$that = $this;
		$numart = $request->getAttribute('id');
		$result = Controllers::Consultar($that,'SELECT * FROM arts WHERE numart='.$numart);
		
		$foto = $result[0]['foto'];
		if($foto!=''){
			$result[0]['foto'] = 'img/arts/' . $foto;
			$result[0]['precio1'] = number_format($result[0]['precio1'], 2, '.', ',');
		}


		$result[0]['fotos'] = explode("\n", $result[0]['fotos']);
		
		$fotos = $result[0]['fotos'];
		
		$nfotos= count($fotos);
		
		$fot[] = (object)[]; 

		if ($nfotos > 1) {
			for($i= 0; $i < $nfotos; $i++){
				if($fotos[$i]!=''){
					$fot[$i] = (object)[];
					$fot[$i]->nombre = 'img/arts/' . $fotos[$i];
				}
			}	
			array_push($fot, $result[0]['foto']);
			$result[0]['fotos'] = $fot;
		}else{

			$result[0]['fotos'] = "";
		}

		$result[0]['fotos'] = $fot;

		
	    echo json_encode($result);
	}

	public function artByLin ($request,$response,$args) {

		$that = $this;
		$lin = $request->getAttribute('linea');
		$linea = str_pad($lin,5," ");
		$result = Controllers::Consultar($that,'SELECT numart,descrip,unidad,precio1,impuesto1,impuesto2,preciopub,foto FROM arts WHERE linea="'.$linea.'"');

		$nArts = count($result);

		if($nArts>0){
			for ($i=0; $i < $nArts ; $i++) {

				//Quitar decimales
				$result[$i]['precio1'] = number_format($result[$i]['precio1'], 2, '.', ',');

				//Poner direccion a foto
				$foto = $result[$i]['foto'];
				if($foto!=''){
					$result[$i]['foto'] = 'img/arts/' . $foto;
				}
			}	
		}
		
	    echo json_encode($result);
	}

};


?>