<?php

namespace ciaController;

use Interop\Container\ContainerInterface;
use Controllers\Controllers;

class ciaController
{
   
	public $db;

   	public function __construct(ContainerInterface $data) {
       $this->db = $data->get('db');
   	}

	public function cia ($request,$response,$args) {
		$that = $this;
		$result = Controllers::Consultar($that,'SELECT nomcia FROM cia');
	    echo json_encode($result);
	}


};


?>