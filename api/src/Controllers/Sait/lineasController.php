<?php

namespace lineasController;

use Interop\Container\ContainerInterface;
use Controllers\Controllers;

class lineasController
{
   
	public $db;

   	public function __construct(ContainerInterface $data) {
       $this->db = $data->get('db');
   	}

	public function lineas ($request,$response,$args) {
		$that = $this;
		$result = Controllers::Consultar($that,'SELECT numlin,nomlin FROM lineas');
		if(count($result)==0){
			$result= array(
				'numlin' => 'SN',
				'nomlin' => 'No hay Lineas' 
			);
		}
	    echo json_encode($result);
	}

	public function lineaById ($request,$response,$args) {
		$that = $this;
		$numlin = $request->getAttribute('id');
		$result = Controllers::Consultar($that,'SELECT numlin,nomlin FROM lineas WHERE numlin='.$numlin);
	    echo json_encode($result);
	}

};


?>