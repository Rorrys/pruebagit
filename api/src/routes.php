<?php

use loginController\loginController;
use usersController\usersController;
use registerController\registerController;
use forgotController\forgotController;

use ciaController\ciaController;
use artsController\artsController;
use lineasController\lineasController;

$app->group('/arts',    function () use ($app) {

    $app->get('',               artsController::class . ':arts');
    $app->get('/{id}',          artsController::class . ':artById');
    $app->get('/fotos/{id}',    artsController::class . ':artByFotos');
    $app->get('/linea/{linea}', artsController::class . ':artByLin');

});

$app->group('/lineas',  function () use ($app) {

    $app->get('',               lineasController::class . ':lineas');
    $app->get('/{id}',          lineasController::class . ':lineaById');

});

$app->group('/cia',     function () use ($app) {

    $app->get('',               ciaController::class . ':cia');

});

//Rutas para el LOGIN
$app->group('/{rfc}/login', function () use ($app) {

    $app->post('',             loginController::class . ':Login');
    $app->post('/register',    registerController::class . ':Register');
    $app->get('/usuarios',     usersController::class . ':Users');
    $app->get('/usuario/{id}', usersController::class . ':UsersById');
    $app->get('/forgot/password/{email}/user', forgotController::class . ':ForgotPass');
    $app->get('/codigo/{numuser}/cambio/{email}/password/{rfc2}/{codigo}', forgotController::class . ':NewCodigo');
    $app->put('/cambio/contra/{id}', forgotController::class . ':NewPass');
    $app->post('/confirm/user/{email}',       registerController::class . ':ConfirmEmail');

});



$app->group('/nuevo', function () use ($app) {

}