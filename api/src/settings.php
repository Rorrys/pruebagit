<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'determineRouteBeforeAppMiddleware' => true,
        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        // Database connection settings 
        "db" => [
            "host" => "websa003.mysql.guardedhost.com:3306",
            "dbname" => "websa003_saitgateway",
            "user" => "websa003_saitgateway",
            "pass" => "Guayalejo143.16.saitgateway"
        ],
        
        /*
         "db" => [
            "host" => "localhost",
            "dbname" => "tiendaSaitMd3",
            "user" => "md3admin",
            "pass" => "Guayalejo.143.16"
        ]
        */
        /*
         "db" => [
            "host" => "localhost",
            "dbname" => "tiendaSaitMd3",
            "user" => "root",
            "pass" => ""
        ]
        */
    ],
];
