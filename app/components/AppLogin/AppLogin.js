import {Backbone}	  from '../../../vendor/vendor';
import {Marionette} from '../../../vendor/vendor';
import LoginView	  from './Login/View';

export default Marionette.Application.extend({
  region: '#app',
  initialize() {
    
    this.on('start', () => {

      toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-left",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
        
	    this.showView(new LoginView());

    })
  }
});






