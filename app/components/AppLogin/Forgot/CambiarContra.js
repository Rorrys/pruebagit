import {Backbone}   from '../../../../vendor/vendor';


export default function cambiarContra(that){

        $('#nuevaContra').hide();
        $("#contenedor3").show();

        var url = apiWeb+'md3admin/login/cambio/contra';
        //var url = 'http://md3.mx/banregio/api/cambio/contra';
        //var url = 'http://www.md3.mx/tenencia/api/cambio/contra';

        var Model = Backbone.Model.extend({
          idAttribute: "id",
          urlRoot: url
        });
        var contra     = $('#nuevaContrase').val();
        var contraMd5 = md5(contra);
        
        that.modelo = new Model();
        that.password = contraMd5;
        that.id = $('#numuserUsuario').val();

        that.modelo.set('password', that.password);
        that.modelo.set('id', that.id);

        that.modelo.save({},{
           success: function(data){

               var respuesta = data.toJSON();
               var pasar = 'Si';
               
               $("#contenedor3").hide();
               
               //si paso a todas las validaciones lo dejo entrar
               if(pasar=='Si'){
                  $('#nuevaContra').show();
                  $('#nuevaContrase').val('');
                  $('#nuevaContrase2').val('');
                  toastr.success('', 'Contraseña Actualizada!');
                  $('#volverForgot').click();
               }
           },
           error: function(erro){
              //como todos los errores los valide en el success, si no entra en ninguna entonces 
              //hay un error en el servidor o conexion a internet
              $("#contenedor3").hide();
              $('#nuevaContra').show();
              toastr.error('Problemas con el Servidor', 'Favor de Intentar mas tarde');
          }
        });

}