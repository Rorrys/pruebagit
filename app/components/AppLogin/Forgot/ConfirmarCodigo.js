 import {Backbone}   from '../../../../vendor/vendor';

 export default function confirmarCodigo(){

        var codigo  = $('#codigo').val();
        var email   = $('#correoForgot').val();
        var rfc     = $('#rfcUsuario').val();
        var numuser = $('#numuserUsuario').val();
        
        if (codigo.length==0){
            toastr.error('Enviado a ' +email, 'Ingresa el codigo de Seguridad');
            return 
        } 

 		    $('#formNuevaContra').hide();
        $('#nuevaContra').hide();
        $('#actualizarContra').hide();
        $("#contenedor3").show();

        var url     = apiWeb+'md3admin/login/codigo/'+numuser+'/cambio/'+email+'/password/'+rfc+'/'+codigo;
        //var url     = 'http://md3.mx/banregio/api/codigo/'+numuser+'/cambio/'+email+'/password/'+rfc+'/'+codigo;
        //var url     = 'http://www.md3.mx/tenencia/api/codigo/'+numuser+'/cambio/'+email+'/password/'+rfc+'/'+codigo;

        var Model = Backbone.Model.extend({
          urlRoot: url
        });

        var modelo = new Model();
        modelo.fetch({
          success: function(data){

                 var respuesta = data.toJSON();
                 var pasar = 'Si';
                 
                 $("#contenedor3").hide();
                 $('#nuevaContra').show();

                 if(respuesta.codigo=='No'){
                  toastr.error('', 'Codigo de Seguridad Incorrecto');
                  pasar = 'No';
                 }

                 //si paso a todas las validaciones lo dejo entrar
                 if(pasar=='Si'){
                    $('#formCodigo').hide();
                    $('#confirmarCodigo').hide();
                    $('#nuevaContra').show();
                    $('#formNuevaContra').show();
                    $('#actualizarContra').show();
                 }
          },
          error: function(erro){
              //como todos los errores los valide en el success, si no entra en ninguna entonces 
              //hay un error en el servidor o conexion a internet
              $("#contenedor3").hide();
              $('#nuevaContra').hide();
              toastr.error('Problemas con el Servidor', 'Favor de Intentar mas tarde');
          }
        });

 }