import {Backbone}   from '../../../../vendor/vendor';

export default function enviarCodigo(){

        var email  = $('#correoForgot').val();
        
        if (email.length==0){
            toastr.error('', 'Ingresa tu correo');
            return 
        } 

		    $('#parteFormulario').hide();
        $('#nuevaContra').hide();
        $("#contenedor3").show();

        var url    = apiWeb+'md3admin/login/forgot/password/'+email+'/user';
        //var url    = 'http://md3.mx/banregio/api/forgot/password/'+email+'/user';
        //var url    = 'http://www.md3.mx/tenencia/api/forgot/password/'+email+'/user';

        var Model = Backbone.Model.extend({
          urlRoot: url
        });

        var modelo = new Model();
        modelo.fetch({
          success: function(data){

                 var respuesta = data.toJSON();
                 var pasar = 'Si';
                 
                 $("#contenedor3").hide();
                 $('#nuevaContra').hide();

                 if(respuesta.email == 'No'){
                    $('#parteFormulario').show();
                    toastr.error('', 'Email no Existe');
                    pasar = 'No';
                 }

                 //si paso a todas las validaciones lo dejo entrar
                 if(pasar=='Si'){
                      toastr.success(email, 'Introduce el Codigo enviado a:');
                      $('#rfcUsuario').val(respuesta.rfc);
                      $('#numuserUsuario').val(respuesta.numuser);
                      $('#nuevaContra').show();
                 }
          },
          error: function(erro){
              //como todos los errores los valide en el success, si no entra en ninguna entonces 
              //hay un error en el servidor o conexion a internet
              $("#contenedor3").hide();
              $('#nuevaContra').hide();
              $('#parteFormulario').show();
              toastr.error('Problemas con el Servidor', 'Favor de Intentar mas tarde');
          }
        });
}