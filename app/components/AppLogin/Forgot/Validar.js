

export default function validar(){

        var registrar   = 'Si';
        var contra1 = $('#nuevaContrase').val();
        var contra2 = $('#nuevaContrase2').val();
        
        if (contra1.length==0){
            toastr.error('', 'Ingresa tu nueva Contraseña');
            registrar   = 'No';
        } 

        else if (contra2.length==0){
            toastr.error('', 'Confirma tu Contraseña');
            registrar   = 'No';
        }

        else if (contra1!=contra2){
            toastr.error('', 'Contraseñas no Coinciden');
            registrar   = 'No';
        }

        return registrar;
    }