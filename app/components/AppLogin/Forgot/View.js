import {Backbone}      from '../../../../vendor/vendor';
import {Marionette}    from '../../../../vendor/vendor';
import CambiarContra   from './CambiarContra';
import Router          from '../../Router';
import ConfirmarCodigo from './ConfirmarCodigo';
import EnviarCodigo    from './EnviarCodigo';
import LoginView       from '../Login/View';
import Validar		     from './Validar';
import template        from './Template.jst';

export default Marionette.View.extend({
	template: template,
	regions: {
		mainRegion: '#main2'
	},
  ui: {
      'formForgot': '#formForgot'
  },
  events:{
      'click #EnviarLink':       'EnviarCodigo',
      'click #confirmarCodigo':  'ConfirmarCodigo',
      'click #actualizarContra': 'cambiarContraseña',
      'click #cancelarNueva':    'Volver', 
      'click #volverForgot':     'Volver'
  },
  EnviarCodigo: function(){
    EnviarCodigo();
  },
  ConfirmarCodigo: function(){
    ConfirmarCodigo();
  },
  cambiarContraseña: function(){
    var that = this;
    var pasar = Validar();
    if(pasar=='Si'){
      CambiarContra(that);  
    }
  },
  Volver(){

    this.getRegion('mainRegion').show(new LoginView());
    var router = new Router();
        router.navigate('login',{trigger:true});
    
  }
 });






