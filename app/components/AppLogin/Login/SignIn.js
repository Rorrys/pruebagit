import {Backbone}     from '../../../../vendor/vendor';
import {Marionette}   from '../../../../vendor/vendor';
import Router         from '../../Router';

export default function SignIn(){

    $('#parteLogin').hide();
    $('#contenedor').show();

    //var url = 'http://www.md3.mx/tenencia/api/login';
    var url = apiWeb+'md3admin/login';  

    var contra = $('#login-password').val();
    var contraMd5 = md5(contra);

    var formValues = {
        email:    $('#login-email').val(),
        password: contraMd5
    };

    var router = new Router();

    $.ajax({
        url:url,
        type:'POST',
        dataType:"json",
        data: formValues,
        success:function (data) {

              var respuesta = data;
              var pasar = 'Si';

              //Si el correo esta registrado sigo
               if(respuesta.email == 'Si'){

                  //si la contraseña es la correcta sigo
                   if(respuesta.password == 'Si'){

                        //si el estatus es 1 - ('Inactivo')
                        if(respuesta.estatus == '1'){

                            //lo detengo y le digo que tiene que confirmar el correo primero
                            toastr.error('', 'Es Necesario Confirmar tu correo');
                            pasar = 'No';

                            $("#contenedor").hide();
                            $('#parteLogin').show();

                        }                        
                       
                   } else { //si la contraseña es incorrecta le digo que esta mal y lo detengo

                      toastr.error('', 'Contraseña Incorrecta');
                      pasar = 'No';
                      $("#contenedor").hide();
                      $('#parteLogin').show();

                   }

               } else{ //si el correo no esta le digo que tiene que registrarse

                    toastr.error('', 'Usuario no Registrado');
                    pasar = 'No';
                    $("#contenedor").hide();
                    $('#parteLogin').show();

               }

               //si paso a todas las validaciones lo dejo entrar
               if(pasar=='Si'){
                
                    localStorage.apiKey  = JSON.stringify(data.jwt);
                    localStorage.rfc     = data.rfc;
                    localStorage.nomuser  = data.nomuser;
                    localStorage.apellido = data.apellido;
                    localStorage.numuser  = data.numuser;
                    localStorage.nivel    = data.nivel;
                    localStorage.nomcia   = data.nomcia;
                    router.navigate('app',{trigger:true});
               }

            
        },
        error: function(result) {
           //como todos los errores los valide en el success, si no entra en ninguna entonces 
           //hay un error en el servidor o conexion a internet
           $("#contenedor").hide();
            $('#parteLogin').show();
            toastr.error('Problemas con el Servidor', 'Favor de Intentar mas tarde');
        }
    });


    
}