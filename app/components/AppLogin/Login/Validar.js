
import SignIn       from './SignIn';

export default function validarLogin(){

    var entrar      = 'Si';
    var email       = $('#login-email').val();
    var password    = $('#login-password').val();

    if (email.length==0){
        toastr.error('', 'Correo Obligatorio');
        entrar      = 'No';
    } 

    if (password.length==0){
        toastr.error('', 'Contraseña Obligatoria');
        entrar      = 'No';
    } 

    if (entrar == 'Si'){
      SignIn();
    }

}