import {Marionette}     from '../../../../vendor/vendor';
import Router           from '../../Router';
import template         from './Template.jst';
import ForgotPassView   from '../Forgot/View';
import RegisterView     from '../Register/View';
import validarLogin     from './Validar';

export default Marionette.View.extend({
	template: template,
    ui: {
        'formLogin': '#login-form',
        'formRegister': '#register-form'
    },
    regions: {
        mainRegion: '#main'
    },
    events:{
        'click #RegistrateAqui'     : 'registerView',
        'click #btn-login'          : 'Login',
        'click #olvideContra'       : 'OlvideContra'
    },
    registerView: function(e){

        this.getRegion('mainRegion').show(new RegisterView());
        var router = new Router();
        router.navigate('register',{trigger:true});

        $('#EmailAddress').val('');
        $('#rfc').val('');
        $('#FirstName').val('');
        $('#apellido').val('');
        $('#telefono').val('');
        $('#passWord').val('');
        $('#passWord2').val('');

    },
    Login: function(event){

        event.preventDefault();
        $('.alert-error').hide();
        validarLogin();
        
    },
    OlvideContra: function(event){

        this.getRegion('mainRegion').show(new ForgotPassView());
        var router = new Router();
        router.navigate('forgot',{trigger:true});
    }
 });






