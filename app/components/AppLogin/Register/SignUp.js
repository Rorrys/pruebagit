
export default function SignUp(){

    $('#parteRegister').hide();
    $('#contenedor2').show();

    var url = apiWeb+'md3admin/login/register';

    var contra1 = $('#passWord').val();
    var contra2 = $('#passWord2').val();
    var email = $('#EmailAddress').val()

    var formValues = {
        email: $('#EmailAddress').val(),
        nomuser: $('#FirstName').val(),
        passWord: $('#passWord').val(),
        rfc: $('#rfc').val(),
        telefono: $('#telefono').val(),
        apellido: $('#apellido').val()
    };
    
    var request;

    if (!request) {

       request = $.ajax({
            url:url,
            type:'POST',
            dataType:"json",
            data: formValues,
            success:function (data) {
              
               var respuesta = data;
               
               var pasar = 'Si';
               $("#contenedor2").hide();
               $('#parteRegister').show();

               //Si el RFC que esta poniendo no esta dado de alta como una Empresa
               //lo detengo y le digo que tiene que contratar una licencia
               if(respuesta.rfc.existe == 'No'){
                    toastr.error('', 'RFC no autorizado para Registarse');
                    toastr.info('DEMO', 'Puedes Utilizar el RFC de Pruebas:');
                    pasar = 'No';
               }

               //si El email ya esta en otro usuario le digo que no puede usarlo
               if(respuesta.email.existe == 'Si'){
                    toastr.error('', 'Email en Uso por otro Usuario');
                    pasar = 'No';
               }

               //si paso alas validaciones entonces lo registro
               if(pasar=='Si'){
                    //location.reload()
                    toastr.success(email, 'Correo de Confirmacion Enviado a');
                    
                    $('#EmailAddress').val('');
                    $('#rfc').val('');
                    $('#FirstName').val('');
                    $('#apellido').val('');
                    $('#telefono').val('');
                    $('#passWord').val('');
                    $('#passWord2').val('');

                    $('#VolverLogin').click();
               }
                
            },
            error: function(result) {
               //como todos los errores los valide en el success, si no entra en ninguna entonces 
               //hay un error en el servidor o conexion a internet
               $("#contenedor2").hide();
               $('#parteRegister').show();
               toastr.error('Problemas con el Servidor', 'Favor de Intentar mas Tarde');
            },complete: function(){
              request = null;
            }
        });
    }
}