
import SignUp       from './SignUp';

export default function validarRegistro(){

        var registrar   = 'Si';
        var email       = $('#EmailAddress').val();
        var rfc         = $('#rfc').val();
        var telefono    = $('#telefono').val();
        var apellido    = $('#apellido').val();
        var nomuser     = $('#FirstName').val();
        var passWord    = $('#passWord').val();
        var passWord2    = $('#passWord2').val();

        if (email.length==0){
            toastr.error('', 'Correo Obligatorio');
            registrar   = 'No';
        } 

        else if (rfc.length==0){
            toastr.error('', 'RFC Obligatorio');
            registrar   = 'No';
        } 

        else if (nomuser.length==0){
            toastr.error('', 'Nombre Obligatorio');
            registrar   = 'No';
        }

        else if (apellido.length==0){
            toastr.error('', 'Apellido Obligatorio');
            registrar   = 'No';
        } 
        
        else if (telefono.length==0){
           toastr.error('', 'Telefono Obligatorio');
           registrar   = 'No';
        } 

        else if (passWord.length==0){
            toastr.error('', 'Contraseña Obligatoria');
            registrar   = 'No';
        }

        else if (passWord2.length==0){
            toastr.error('', 'Confirma tu Contraseña');
            registrar   = 'No';
        }

        else if(passWord!=passWord2){
            toastr.error('', 'Contraseñas no Coindicen');
            registrar   = 'No';
        } 

        if (registrar == 'Si'){
          SignUp();
        }
    }