import {Marionette}     from '../../../../vendor/vendor';
import template         from './template.jst';
import Router           from '../../Router';
import LoginView        from '../Login/view';
import validarRegistro  from './Validar';

export default Marionette.View.extend({
	template: template,
    ui: {
        'formRegister': '#register-form'
    },
    regions: {
        mainRegion: '#main2'
    },
    events:{
        'click #VolverLogin' : 'loginView',
        'click #btn-signup'  : 'Registro'
    },
    loginView: function(e){

        this.getRegion('mainRegion').show(new LoginView());
        var router = new Router();
        router.navigate('login',{trigger:true});

    },
    Registro: function(event){

        event.preventDefault();
        $('.alert-error').hide();
        validarRegistro();
    }
 });






