import {Backbone}   from '../../../vendor/vendor';
import {Marionette} from '../../../vendor/vendor';
import MenuView		 from './Menu/View';

var App = Marionette.Application.extend({
  region: '#app',
  initialize() {
    this.on('start', () => {

      var that = this;
      var contador=1;

      //Consultar lineas que existen en la empresa
      var LineasCollection = Backbone.Collection.extend({
          url: apiWeb+'lineas'
      });
      
      var collection = new LineasCollection();
      collection.fetch({
          success: function(data){

              that.showView(new MenuView({collection: collection}));
              var nombreEmpresa = $('<a style="color:#f5f5f5;" id="nomcia" href="">'+localStorage.nomcia+' </a>');
              $('#divNameEmpresa').append(nombreEmpresa);

              $('[data-toggle="offcanvas"]').click(function () {
                  
                  $('#sidebar').show();
                  $('#infoArts').show();
                  $('#menuLateralCarrito').hide();
                  $('.row-offcanvas').toggleClass('active');
                  
              });

              if($("[data-toggle='offcanvas']").is(":visible")){
                $('[data-toggle="offcanvas"]').click();
              }

          }
      });

    });

  }
});

export default App;