import {Marionette} from '../../../../vendor/vendor';
import template		from './template.jst';

export default Marionette.View.extend({
	template: template,
	events: {
		'click .imgArt': 'ImagenPrincipal'
	},
	ImagenPrincipal(event){
		event.preventDefault();
		var id = event.currentTarget.id;
		$("#ImgPrincipal").attr("src",id);
	}	
});






