import {Marionette} from '../../../../vendor/vendor';
import TotalesView  from './Totales/View';
import template		from './template.jst';

export default Marionette.View.extend({
	template: template,
	regions: {
		'CarritoTotales': '#CarritoTotales'
	},
	events: {
		'click #verTotalesCarrito': 'verTotalesCarrito'
	},
	verTotalesCarrito(){
		var totales = this.options.totales;
		this.getRegion('CarritoTotales').show(new TotalesView({collection: totales}));
	}
});






