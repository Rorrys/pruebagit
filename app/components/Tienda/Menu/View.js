import {Backbone}   from '../../../../vendor/vendor';
import {Marionette} from '../../../../vendor/vendor';
import NumberFormat from './functions/number_format';
import AddArt       from './functions/AddArt';
import RemoveArt    from './functions/RemoveArt';
import VerArticulo  from './functions/VerArticulo';
import ArtsDeLaLinea from './functions/ArtsDeLaLinea';
import ActualizarCarrito from './functions/ActualizarCarrito';
import CarritoView  from '../Carrito/View';
import template		  from './template.jst';

export default Marionette.View.extend({
	template: template,
  regions: {
		menuLateralDerechoRegion: '#menuLateralDerecho',
    MainPrincipalRegion:      '#MainPrincipal'
	},
  events: {
    'click #liCarritoImg'     : 'VistaCarrito',
    'click .list-group-item'  : 'ArtsDeLaLinea', 
    'click .AddArt'           : 'AddArt',    //Agregado desde el carrrito
    'click .RemoveArt'        : 'RemoveArt', //Eliminado desde el Carrito
    'click .Agregar'          : 'AddArt',     //Agregado desde la lista de Aritucos
    'click .VerArticulo'      : 'VerArticulo',//Agregado desde la lista de Aritucos
    'click #CerrarInfoArt'    : 'CerrarInfoArt'
  },
  ArtsDeLaLinea(event){
      event.preventDefault();
      var that = this;
      ArtsDeLaLinea(that,event);
      $('#menuLateralDerecho').hide();
  },
  AddArt(event){
      event.preventDefault();
      var that = this;
      AddArt(that,event);
      $('#menuLateralDerecho').hide();
  },
  RemoveArt(event){
      event.preventDefault();
      var that = this;
      RemoveArt(that,event);
      $('#menuLateralDerecho').hide();
  },
  VerArticulo(event){
      event.preventDefault();
      var that = this;
      VerArticulo(that,event);
  },
  VistaCarrito(event){
    //detengo el evento del click para evitar que se recarge la pagina
    if(event!=undefined){event.preventDefault();}

    //Si lo esta viendo desde un celular 
    if($("[data-toggle='offcanvas']").is(":visible")){
      //oculto el menu de arriba
      $('#botonNavbarTop').click();
      $('#menuLateralDerecho').hide();
      //oculto el menu lateral
      $('.row-offcanvas').removeClass("active");
      
    }
    
    //Creo mi Array que voy a mandarle a la collecion de la vista
    var arrayTotales  = new Array({
      'total': NumberFormat(Number(this.subtotal)+Number(this.iva)),
      'subtotal' : NumberFormat(this.subtotal),
      'iva': NumberFormat(this.iva)
    });
   
    //Crea la collecino para mandarsela a la vista con los datos
    var articulos = new this.Articulos(this.arts);
    var totales = new this.Totales(arrayTotales);

    //Ejecuto la vista del carrito
    this.getRegion('MainPrincipalRegion').show(new CarritoView({collection: articulos, totales: totales}));
    $('#verTotalesCarrito').click();
    $('#menuLateralDerecho').hide();
  },
  initialize(){

    //Esta funcion Javascript es como una consulta SQL a un Array, busca un objeto que contenga lo que le mandes
    Array.prototype.indexOfObject = function arrayObjectIndexOf(property, value) {
        //recorre el objeto buscando la propiedad que le mandaste
        for (var i = 0, len = this.length; i < len; i++) {
          //Si encontro lo que le mandaste regresa 1
            if (this[i][property] === value) return i;
        }
        //Si no lo encontro regresa -1
      return -1;

    }

    //DEFINO VARIABLES  
    this.subtotal = 0;              //subtotal de la venta
    this.iva      = 0;              //subtotal de la venta
    this.total    = 0;              //total de la venta
    this.arts     = new Array();    //Articulos que lleva agregados a su carrito
    this.artsLinea= new Array();    //Artiuclos que pertecen a la linea que esta consultando
    this.Articulos= Backbone.Collection.extend();  //defino la collecion de articulos que van ala vista del carrito
    this.Totales  = Backbone.Collection.extend();  //defino la collecion de totales que van ala vista del carrito 
  }
});