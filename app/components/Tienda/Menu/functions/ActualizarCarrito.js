import NumberFormat from './number_format';

export default function ActualizarCarrito(that,Clase){

      //Lo defino en cero los totales porque los vuelvo a calcular
      that.iva=0;
      that.subtotal=0;

      //Recorri mi array de aritculo en el carrito
      for (var i = 0; i<that.arts.length; i++) {
          //Sumo los iva de cada partida
          that.iva = Number(that.iva) + Number(that.arts[i].iva.replace(",", ""));
          //Suma los precios de cada partida
          that.subtotal = Number(that.subtotal) + Number(that.arts[i].total.replace(",", ""));
      }

      //Ver si lo agrego desde el carrito     
      if(Clase=='AddArt' || Clase=='RemoveArt'){
          that.VistaCarrito();
      }
    
      
  }