import ActualizarCarrito from './ActualizarCarrito';
import NumberFormat from './number_format';

export default function AddArt(that,event){

      var cantidad = 1;

      var element = event.currentTarget;

      //Obtener el NUMART del articulo      
      var Numart = event.currentTarget.id;

      //Quitarle los espacios en blanco
      var numart = Numart.trim();

      //Buscar si ya tenia uno de estos articulos en su carrito, porque si ya esta, solo aumento cantidad
      var posicion = that.arts.indexOfObject("numart", numart);

      //Si lo encontro
      if(posicion != -1){

          //Aumento cantidad
          that.arts[posicion].cantidad = that.arts[posicion].cantidad + 1;
          //Aumento iva
          that.arts[posicion].total    = NumberFormat(that.arts[posicion].cantidad * Number(that.arts[posicion].precio1.replace(",", "")));
          that.arts[posicion].iva    = NumberFormat(Number(that.arts[posicion].total.replace(",", "")) * (Number(that.arts[posicion].impuesto1)/100));
          
      }
      //Si no esta en su carrito lo agrego como nuevo
      else{

          //Buscar su informacion en mi lista de articulos (descripcion y precio)
          var nArt = that.artsLinea.indexOfObject("numart", numart);

          //Si lo encontro
          if(nArt != -1){
              
              var impuesto = (Number(that.artsLinea[nArt].impuesto1) / 100);
              var iva = Number(that.artsLinea[nArt].precio1)*impuesto;
              
              //Llenar mi objeto para agregarlo a la coleccion que se manda ala vista
              var articulo = {
                'numart': numart,
                'cantidad' : cantidad,
                'descrip': that.artsLinea[nArt].descrip,
                'precio1': NumberFormat(that.artsLinea[nArt].precio1),
                'impuesto1': NumberFormat(that.artsLinea[nArt].impuesto1),
                'impuesto2': NumberFormat(that.artsLinea[nArt].impuesto2),
                'unidad': NumberFormat(that.artsLinea[nArt].unidad),
                'preciopub': NumberFormat(that.artsLinea[nArt].preciopub),
                'subtotal': NumberFormat(that.artsLinea[nArt].precio1),
                'iva': NumberFormat(iva),
                'total': NumberFormat(that.artsLinea[nArt].precio1)
              };  
              
              //Agregarlo al array de artiuclos 
              that.arts.push(articulo);

              //Indicar en mi vista de Articulos que ya esta Agregado
              $(element).text('Agregado');
              $(element).attr('disabled',true);
              $(element).removeClass();
              $(element).addClass('btn btn-success');
              
          }  

      }

      //Actualizo vista de carrito
      //Ver si lo agrego desde el carrito     
      var Clase = event.currentTarget.className;
      ActualizarCarrito(that, Clase);
      toastr.clear();
      toastr.success('Articulo Agregado');

}