import {Backbone}   from '../../../../../vendor/vendor';
import {Marionette} from '../../../../../vendor/vendor';
import ArtsView     from '../../Arts/View';

export default function ArtsDeLaLinea(that,event){
	
	//Del evento click puedo obtener el id del boton
        var linea = event.currentTarget.id;
        var nomlinea = event.currentTarget.innerText;
        
        //Buscar esa linea en mi base de datos para obtener los articulos que tiene 
        var LineasCollection = Backbone.Collection.extend({
            url: apiWeb+'arts/linea/' + linea 
        });
        
        //Ejecutando consulta
        var collection = new LineasCollection();
        collection.fetch({
            success: function(data){

                //Convertir datos a json
                var datos = data.toJSON();

                //Definiendo mis articulos de esta linea
                that.artsLinea = datos;

                //Mostrar los articulos de la linea seleccionada
                that.getRegion('MainPrincipalRegion').show(new ArtsView({collection: collection}));

                //Poner el nombre de la linea en la lista de Articulos
                var nombrelinea = $('<h2 class="sub-header">'+nomlinea+'</h2>');
                $('#NombreLinea').append(nombrelinea);
               
                //Por cada articulo de esa linea debo buscar cual fue agregado al carrito
                //Para indicar que ya esta agregado en la vista
                $('.Agregar').each(function(key, element){

                    //Definir el NumArt 
                    var Numart = element.id;
                    var numart = Numart.trim();
                    
                    //Buscar si esta el articulo agregado al carrito
                    var encontro = that.arts.indexOfObject("numart", numart);

                    //Si lo encontro
                    if(encontro!=-1){
                      //Indicar en mi vista de Articulos que ya esta Agregado
                      $(element).text('Agregado');
                      $(element).attr('disabled',true);
                      $(element).removeClass();
                      $(element).addClass('btn btn-success');
                    }

                     
                });

                //Si es desde el celular mostrar automatiamente el Menu de Lineas
                if($("[data-toggle='offcanvas']").is(":visible")){
                    $('[data-toggle="offcanvas"]').click();
                }
               
            }
        });
	
}