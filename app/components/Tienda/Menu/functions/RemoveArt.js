import NumberFormat from './number_format';
import ActualizarCarrito from './ActualizarCarrito';

export default function RemoveArt(that,event){
	
	var cantidad = 1;

      //Obtener el NUMART del articulo      
      var Numart = event.currentTarget.id;

      //Quitarle los espacios en blanco
      var numart = Numart.trim();

      var element = document.getElementById(Numart);

      //Buscar si ya tenia uno de estos articulos en su carrito, porque si ya esta, solo aumento cantidad
      var posicion = that.arts.indexOfObject("numart", numart);

      //Si lo encontro
      if(posicion != -1){

          //Disminuyo cantidad cantidad
          that.arts[posicion].cantidad = that.arts[posicion].cantidad - 1;
          //Calculo Totales iva
          that.arts[posicion].total    = NumberFormat(that.arts[posicion].cantidad * Number(that.arts[posicion].precio1.replace(",", "")));
          that.arts[posicion].iva    = NumberFormat(Number(that.arts[posicion].total.replace(",", "")) * (Number(that.arts[posicion].impuesto1)/100));

      }

      //Si su cantidad lllego a cero
      if(that.arts[posicion].cantidad==0){
        //Elimnarlo de mi arreglo de articulos del carrito
        that.arts.splice(posicion,1);

        //Habilitarlo para que lo puedan volver agregar desde la lista de articulos
        $(element).text('Agregar');
        $(element).attr('disabled',false);
        $(element).removeClass();
        $(element).addClass('btn btn-info Agregar');

      }

      //Actualizo vista de carrito
      var Clase = event.currentTarget.className;
      ActualizarCarrito(that,Clase);
      toastr.clear();
      toastr.success('Articulo Eliminado');

}