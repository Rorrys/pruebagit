import {Backbone}   from '../../../../../vendor/vendor';
import {Marionette} from '../../../../../vendor/vendor';
import ArticuloView from '../../Articulo/View';

export default function VerArticulo(that,event){
		
	 var cantidad = 1;

      var element = event.currentTarget;

      //Obtener el NUMART del articulo      
      var Numart = event.currentTarget.id;

      //Quitarle los espacios en blanco
      var numart = Numart.trim();

      //Buscar esa linea en mi base de datos para obtener los articulos que tiene 
      var ArtCollection = Backbone.Collection.extend({
          url: apiWeb+'arts/fotos/' + numart 
      });
      
      //Ejecutando consulta
      var collection = new ArtCollection();
      collection.fetch({
          success: function(data){

              that.getRegion('menuLateralDerechoRegion').show(new ArticuloView({collection: collection}));
              $('#menuLateralDerecho').show();
          }
      });


}