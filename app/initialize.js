import {Backbone} from '../vendor/vendor';
import TiendaApp  from './components/Tienda/App';

export default  document.addEventListener('DOMContentLoaded', () => {

    //Consultar el nombre de la empresa
    var Cia = Backbone.Collection.extend({
        url: apiWeb+'cia'
    });

    var cia = new Cia();

    //Colocar el nomrbe de la empresa
    cia.fetch({
        success: function(data){
            var datos = data.toJSON();
            localStorage.nomcia = datos[0].nomcia;
        }
    });

		var tiendaApp = new TiendaApp();
    tiendaApp.start();
    	
    toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "200",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
    }
    	
});



