
var gulp          = require('gulp');
var uglify        = require('gulp-uglify');
var streamify     = require('gulp-streamify');
var source        = require('vinyl-source-stream');
var buffer        = require('vinyl-buffer');
var browserify    = require('browserify');
var browserSync   = require('browser-sync').create();
var js_obfuscator = require('gulp-js-obfuscator');
var request       = require('request');

var watch = function() {
 browserSync.init({
   server: './public'
 });
 gulp.watch('app/**/*', ['build']);
 gulp.watch('public/*').on('change', browserSync.reload);
};

var build = function () {
   // set up the browserify instance on a task basis
   var b = browserify('./app/initialize.js', {
     transform: [
       ['babelify', {
         'presets': ['es2015']
       }],
       ['jstify']
     ]
   });

  copy();

   return b.bundle()
     .pipe(source('app.js'))
     .pipe(buffer())
     .pipe(uglify())
     .pipe(js_obfuscator({} ))
     .pipe(gulp.dest('./public'))
     .pipe(browserSync.stream());
};

var copy = function () {
   return gulp
     .src(['app/assets/index.html', 'app/styles/**/*', 'app/libs/**/*'])
     .pipe(gulp.dest('./public'))
     .pipe(browserSync.stream());
};

var start = function () {
 build();
 watch();
};

gulp.task('build', function() { return build(); });
gulp.task('start', function() { return start(); });