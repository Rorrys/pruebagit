import _ from  'underscore';
import Backbone from 'backbone';
import $ from 'jquery';
import Marionette from 'backbone.marionette';
Backbone.$ = $;
window.$ = $;
window.apiWeb = 'http://localhost/tienda/api/public/';
//window.apiWeb = 'http://www.saitmd3.com/api/public/';
//window.apiWeb = 'https://soportesait.com/api/public/'
export {_, $, Backbone, Marionette};